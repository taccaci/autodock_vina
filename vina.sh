# Agave parameter
paramlist=`basename ${paramFile}`
cp ${paramFile} ./

if [ ! -f ${proteinFile} ]; then
  echo "ERROR: The protein file ${proteinFile} was not found."
  exit 1
fi

# inject all the other Agave parameters straight into the config.in file
cat > ./config.in << 'EOF'
receptor       = ${proteinFile}
center_x       = ${centerX}
center_y       = ${centerY}
center_z       = ${centerZ}
size_x         = ${sizeX}
size_y         = ${sizeY}
size_z         = ${sizeZ}
num_modes      = 1
exhaustiveness = 10
cpu            = 1
EOF

# output from vina gets shuffled into an appropriate subdirectory, but vina
# won't create the directory if it doesn't exist.  Thus, we need to recreate
# the directory structure

libraryDir=${paramFile}
libraryDir=${libraryDir%/*}
for dir in $(ls -d $libraryDir/*/); do
  mkdir -p ./out/$(basename ${dir%/})
done

if [ ! -d results ]; then
  mkdir results
fi

if [ -z "$TACC_LAUNCHER_DIR" ]; then
  echo "WARNING: launcher was not loaded"
  module load launcher
fi

export EXECUTABLE=$TACC_LAUNCHER_DIR/init_launcher 
export CONTROL_FILE=${paramlist}
export WORKDIR='.'

#----------------
# Error Checking
#----------------

if [ ! -e $WORKDIR ]; then
    echo "Error: unable to change to working directory."
    echo "       $WORKDIR"
    echo "Job not submitted."
    exit 2
fi

if [ ! -f $EXECUTABLE ]; then
    echo "Error: unable to find launcher executable $EXECUTABLE."
    echo "Job not submitted."
    exit 3
fi

if [ ! -f $WORKDIR/$CONTROL_FILE ]; then
    echo "Error: unable to find input control file $CONTROL_FILE."
    echo "Job not submitted."
    exit 4
fi


#----------------
# Job Submission
#----------------
date
echo "Docking ligands ..."
time $TACC_LAUNCHER_DIR/paramrun $EXECUTABLE $CONTROL_FILE

echo "Docking complete."

# set system variables.  You can remove this if they are set elsewhere.
OUT_DIR="./out"
TEMP_RESULTS_PREFIX="./out/tempResults"
REPORT_FILE="topResults.txt"
NUM_TOP_RESULTS=1000
TOP_RESULTS_DIR="./results"
MY_GREP=`which grep`
MY_AWK=`which awk`
MY_CUT=`which cut`


# combine results files
TEMP_FILE=${TEMP_RESULTS_PREFIX}.txt
cat ${TEMP_RESULTS_PREFIX}* > $TEMP_FILE

# Sort that file.
echo -n "Sorting Results ..."
sort -n -k2,2 -o $TEMP_FILE $TEMP_FILE
echo " done"

# get the top results
echo -n "Getting Top Results ..."
# this sed command deletes lines starting from $NUM_TOP_RESULTS + 1 to the end of the file
eval "sed -i '$(($NUM_TOP_RESULTS + 1)),\$d' $TEMP_FILE"
echo " done"

# copy all the top ligands to the results folder
echo -n "Copying Files ..."
while read line
do
  ligFile=${line%% *}
  ligName=$(basename ${ligFile%%.pdbqt*})
# JMF note: I don't think we need to give back the log file.  It's only helpful if there are errors.
#   lgLogName="$ligName.log"

  echo "$ligName  ${line#* }" >> $TOP_RESULTS_DIR/$REPORT_FILE
  $MY_CUT -c-66 $ligFile > $TOP_RESULTS_DIR/$ligName.pdb
#   cp $OUT_DIR/$lgLogName $TOP_RESULTS_DIR/$lgLogName
done < $TEMP_FILE
# rm $TEMP_FILE
echo " done"

echo -n "Making zip file ..."
cp config.in $TOP_RESULTS_DIR
zip -r results.zip $TOP_RESULTS_DIR
echo " done"
date

