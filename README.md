===============================
TACC Autodock Vina Scientific App Repository
===============================

The goal of hte Scientific Apps repository is to create a repository of Docker_ images
for scientific applications.  The main home of created images is the
`Docker hub`_.  The work here does not try to replicate such content.
We want to add some features and conveniences for developers to the
subset of applications most commonly used by scientists in the TACC
environment.

The features in the current images are:

- **Included documentation**: we define a standard way to add
  documentation and examples to the image.  An containerized
  application in this framework automatically accepts a set of common
  options, such as ``-h`` for help, ``-e`` for examples of use,
  etc. (see :ref:`using`).

- **Attribution metadata**: an uniform way to specify version,
  authors, license, and other metadata.  This can be retrieved through
  command line options, same as the documentation.

- **Analytics**: the containers can automatically send information
  about the application being run to a central server, to collect
  usage and analytics.  This feature can be disabled, if necessary.

- **Developer friendliness**: we want to make it easy for developers
  to extend the images. The images provide simple ways to translate
  the above features to new images through inheritance. It is easy to
  add background processes and to set the environment for an
  application.

Quickstart
==========

Using the Autodock Vina sci-app image
---------------------

The image is currently built and hosted at the taccsciapps_ organization
at the `Docker hub`_.

Here is an example of discovery and of usage of the features for the
image `taccsciapps/autodock-vina`:

Running the image with no options displays a basic help of usage:

```bash

   $ docker run -it --rm taccsciapps/autodock-vina

```  

The help includes the help of each of the sci-apps from which the
image inherits.

Display the common flags for the different types of help with:

```bash

   $ docker run -it --rm taccsciapps/autodock-vina -h

```  

The flag ``-e`` shows examples of usage that can be copy and pasted
for immediate experimentation. For example:

```bash

   $ docker run -it --rm taccsciapps/autodock-vina -e

```

shows how to run a docking demo bundled with the image.


Extending this image
-------------------------

Running the image with the ``-d`` flag shows the documentation
included in the image. In particular, it will show the documentation
for the base image ``taccsciapps/base``, which details how to extend
it.

The Dockerfiles for inspecting and modifying existing images can be
found at the `taccaci Bitbucket repository`_.

* Docker hub ([https://registry.hub.docker.com/repos/taccsciapps/](https://registry.hub.docker.com/repos/taccsciapps/))
* Docker ([http://docker.com](http://docker.com))
* taccsciapps  ([https://registry.hub.docker.com/repos/taccsciapps/](https://registry.hub.docker.com/repos/taccsciapps/))
* taccaci Bitbucket repository ([ https://bitbucket.org/taccaci/sci-apps/src](https://bitbucket.org/taccaci/sci-apps/src))
